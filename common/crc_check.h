#ifndef __CRC_CHECK_HEADER
#define __SOBEL_CORE_HEADER

#include "stdint.h"
#include "stdlib.h"

/*
 * Calculate CRC checksum.
 * Initial CRC can be 0 for 
 */
static inline uint32_t crc32(uint32_t crc, const uint8_t *buf, size_t len)
{
    uint32_t table[256];
    int have_table = 0;
    uint32_t rem;
    uint8_t octet;
    int i, j;
    const uint8_t *p, *q;
 
    /* This check is not thread safe; there is no mutex. */
    if (have_table == 0) {
        /* Calculate CRC table. */
        for (i = 0; i < 256; i++) {
            rem = i;  /* remainder from polynomial division */
            for (j = 0; j < 8; j++) {
                if (rem & 1) {
                    rem >>= 1;
                    rem ^= 0xedb88320;
                } else
                    rem >>= 1;
            }
            table[i] = rem;
        }
        have_table = 1;
    }
    
    crc = ~crc;
    q = buf + len;
    for (p = buf; p < q; p++) {
        octet = *p;  /* Cast to unsigned octet. */
        crc = (crc >> 8) ^ table[(crc & 0xff) ^ octet];
    }
    return ~crc;
}

#endif // __CRC_CHECK_HEADER