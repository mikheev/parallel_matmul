#include "acutest.h"
#include "parallel_matmul_core.h"

void test_m_idx(void)
{
    //            Cols: 0  1  2  3  4   // Lines
    uint8_t matrix[] = {0, 0, 0, 0, 0,  // 0
                        0, 0, 1, 0, 0,  // 1
                        0, 0, 0, 0, 0}; // 2

    uint32_t width = 5;
    uint32_t line = 1;
    uint32_t col = 2;
    uint8_t expected_pix_value = 1;

    uint32_t idx = m_idx(width, line, col);

    TEST_CHECK(matrix[idx] == expected_pix_value);
}


void multiply_matrix_test(void)
{
    uint16_t matrix_a[] = {1, 2, 3,
                           4, 5, 6};
    uint16_t matrix_b[] = {5, 4, 2, 1,
                           9, 4, 6, 1,
                           2, 2, 4, 7};
    uint16_t matrix_c[] = {0, 0, 0, 0,
                           0, 0, 0, 0};
    uint16_t expected_matrix_c[] = {29, 18, 26, 24,
                                    77, 48, 62, 51};
    multiply_matrix(matrix_a, matrix_b, matrix_c, 2, 3, 4);

    for (int i = 0; i < 8; i++)
    {
        TEST_CHECK(matrix_c[i] == expected_matrix_c[i]);
    }
}

TEST_LIST = {
    {"getting matrix buf indexes", test_m_idx},
    {"matrix multiplication test", multiply_matrix_test},
    {NULL, NULL }
};