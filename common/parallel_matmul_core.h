#ifndef __PARALLEL_MATMUL_CORE_HEADER
#define __PARALLEL_MATMUL_CORE_HEADER

#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"

/*
 * Calculate index for point with coordinates (x, y) of 2D image
 * in 1D matrix buffer. 
 */ 
#define m_idx(width, line, column) ((line) * (width) + (column))

static inline void multiply_matrix(volatile uint16_t * matrix_a, volatile uint16_t * matrix_b, volatile uint16_t * matrix_c,
                                   uint32_t height_a, uint32_t width_a_height_b, uint32_t width_b)
{
    register uint32_t i, j, k;
    register uint16_t sum;
    for (i = 0; i < height_a; i++)
    {
        for (j = 0; j < width_b; j++)
        {
            // С_ij = Sum_k A_ik B_kj
            sum = 0;
            for (k = 0; k < width_a_height_b; k++)
            {
                sum += matrix_a[m_idx(width_a_height_b, i, k)] * matrix_b[m_idx(width_b, k, j)];
            }
            matrix_c[m_idx(width_b, i, j)] = sum;
        }
    }
}

static inline void print_matrix(uint16_t * matrix, uint32_t matrix_width, uint32_t matrix_height)
{
    for (int i = 0; i < matrix_height; i++)
    {
        for (int j = 0; j < matrix_width; j++)
        {
            printf("%4d ", matrix[m_idx(matrix_width, i, j)]);
        }
        printf("\n");
    }
}

#endif // __PARALLEL_MATMUL_CORE_HEADER