#include "acutest.h"
#include "crc_check.h"

void test_crc32(void) { 
    uint8_t test_arr[] = {1, 2, 3, 4, 5, 6, 7, 8};
    const uint32_t crc_expected = 0x3fca88c5;
    uint32_t crc;

    crc = crc32(0, test_arr, 8);

    TEST_CHECK(crc == crc_expected);
}

void test_crc32_16_bit(void) { 
    uint16_t test_arr[] = {1, 2, 3, 4, 5, 6, 7, 8};
    const uint32_t crc_expected = 0x167f332b;
    uint32_t crc;

    crc = crc32(0, (uint8_t *)test_arr, 8 * sizeof(uint16_t));

    TEST_CHECK(crc == crc_expected);
}


TEST_LIST = {
    {"crc32", test_crc32},
    {"crc32 16 bit array", test_crc32_16_bit},
    {NULL, NULL }
};