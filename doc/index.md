# Общие положения

Работаем с квадратными матрицами.

Тип данных - `uint16_t`.

Значения исходных матриц должны находиться в диапазоне `[0, 255]`, чтобы результат произведения уложился в `uint16_t`.



## Граничные точки

### Эталонная реализация на языке Python

```python

```

## Порядок тестирования

### Входные и выходные данные

### Измерения

### Проверка результата

## Ссылки

<https://docs.opencv.org/4.1.0/d3/d94/samples_2cpp_2tutorial_code_2ImgTrans_2Sobel_Demo_8cpp-example.html#a13>
<https://docs.opencv.org/4.1.0/d4/d86/group__imgproc__filter.html#gacea54f142e81b6758cb6f375ce782c8d>
<https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_imgproc/py_gradients/py_gradients.html>

