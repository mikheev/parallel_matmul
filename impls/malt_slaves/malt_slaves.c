#include "malt_ext.h"
#include "stdio.h"
#include "stdbool.h"
#include "string.h"

#include "../../common/parallel_matmul_core.h"
#include "../../common/crc_check.h"
#include "../../common/lwlog.h"

//#include "../../test_data/matrix_16_lines.h"
//#include "../../test_data/matrix_32_squares.h"
#include "../../test_data/matrix_512_random.h"

#define BLOCK_DIM          16

#define MEM_ALIGNMENT      16

// Calculation settings
#define READ_GLOBAL_MEM_ENABLE
#define PROCESS_DATA_ENABLE
#define WRITE_GLOBAL_MEM_ENABLE
#define CHECK_CRC

#define MOVE_GLOBAL_BUFFERS_TO_HEAP

//#define PRINT_RESULT_MATRIX


uint16_t input_matrix_a[] __attribute__ ((aligned (MEM_ALIGNMENT))) = MATRIX_A;
uint16_t input_matrix_b[] __attribute__ ((aligned (MEM_ALIGNMENT))) = MATRIX_B;
uint16_t static_matrix_c[MATRIX_DIM * MATRIX_DIM] __attribute__ ((aligned (MEM_ALIGNMENT)));
uint16_t static_processed_blocks_storage[BLOCK_DIM * BLOCK_DIM * (MATRIX_DIM / BLOCK_DIM)] __attribute__ ((aligned (MEM_ALIGNMENT)));

uint32_t global_sync_arr[128];

////////  Slave functions  ///////////


static inline void slave_multiply_blocks(volatile uint16_t * matrix_a, volatile uint16_t * matrix_b,
                                         uint32_t block_a_idx_x, uint32_t block_a_idx_y,
                                         uint32_t block_b_idx_x, uint32_t block_b_idx_y,
                                         volatile uint16_t * processed_blocks_storage)
{
    volatile uint16_t block_a[BLOCK_DIM * BLOCK_DIM];
    volatile uint16_t block_b[BLOCK_DIM * BLOCK_DIM];
    volatile uint16_t block_c[BLOCK_DIM * BLOCK_DIM];

    #ifdef READ_GLOBAL_MEM_ENABLE
    for (uint32_t i = 0; i < BLOCK_DIM; i++)
    {
        const uint32_t src_a_offset = block_a_idx_y * BLOCK_DIM * MATRIX_DIM + block_a_idx_x * BLOCK_DIM;
        const uint32_t src_b_offset = block_b_idx_y * BLOCK_DIM * MATRIX_DIM + block_b_idx_x * BLOCK_DIM;
        malt_memcpy((volatile void *)&block_a[i * BLOCK_DIM], (volatile void *)&matrix_a[src_a_offset + i * MATRIX_DIM], BLOCK_DIM * sizeof(uint16_t));
        malt_memcpy((volatile void *)&block_b[i * BLOCK_DIM], (volatile void *)&matrix_b[src_b_offset + i * MATRIX_DIM], BLOCK_DIM * sizeof(uint16_t));
    }
    #endif  // READ_GLOBAL_MEM_ENABLE

    #ifdef PROCESS_DATA_ENABLE
    multiply_matrix(block_a, block_b, block_c, BLOCK_DIM, BLOCK_DIM, BLOCK_DIM);
    #endif  // PROCESS_DATA_ENABLE

    #ifdef WRITE_GLOBAL_MEM_ENABLE
    for (uint32_t i = 0; i < BLOCK_DIM; i++)
    {
        const uint32_t dst_offset = block_a_idx_x * BLOCK_DIM;
        malt_memcpy((volatile void *)&processed_blocks_storage[dst_offset + i * MATRIX_DIM], (volatile void *)&block_c[i * BLOCK_DIM], BLOCK_DIM * sizeof(uint16_t));
    }
    #endif  // WRITE_GLOBAL_MEM_ENABLE
}


static inline int slave_multiply_blocks_in_line(int slave_id, int slaves_total, int a_line, int b_col,
                                                uint16_t * matrix_a, uint16_t * matrix_b, uint16_t * matrix_c,
                                                uint16_t * processed_blocks_storage)
{
    const uint32_t num_blocks_y = MATRIX_DIM / BLOCK_DIM;
    
    uint32_t block_n;
    for (block_n = slave_id; block_n < num_blocks_y; block_n += slaves_total)
    {
        slave_multiply_blocks(
            matrix_a, matrix_b,
            block_n, a_line,
            b_col, block_n,
            processed_blocks_storage
        );
    }

    return 0;
}


static inline void slave_sum_block_line(volatile uint16_t * processed_blocks_storage,
                                        uint32_t block_c_idx_x, uint32_t block_c_idx_y,
                                        volatile uint16_t * matrix_c,
                                        uint32_t block_line_idx_y)
{
    volatile uint16_t line[BLOCK_DIM];
    malt_memset((void *) line, 0, BLOCK_DIM * sizeof(uint16_t));
    volatile uint16_t line_temp[BLOCK_DIM];

    for (uint32_t i = 0; i < MATRIX_DIM / BLOCK_DIM; i++)
    {
        #ifdef READ_GLOBAL_MEM_ENABLE
        malt_memcpy(line_temp,
                    &processed_blocks_storage[block_line_idx_y * MATRIX_DIM + i * BLOCK_DIM],
                    BLOCK_DIM * sizeof(uint16_t));
        #endif  // READ_GLOBAL_MEM_ENABLE
        
        #ifdef PROCESS_DATA_ENABLE
        for (uint32_t j = 0; j < BLOCK_DIM; j++)
        {
            line[j] += line_temp[j];
        }
        #endif  // PROCESS_DATA_ENABLE
    }

    #ifdef WRITE_GLOBAL_MEM_ENABLE
    malt_memcpy((volatile void *)&matrix_c[(block_c_idx_y * BLOCK_DIM + block_line_idx_y) * MATRIX_DIM + block_c_idx_x * BLOCK_DIM],
                (volatile void *)line,
                BLOCK_DIM * sizeof(uint16_t));
    #endif  // WRITE_GLOBAL_MEM_ENABLE
}

static inline int slave_sum_block_lines_in_block(int slave_id, int slaves_total, int a_line, int b_col,
                                                 uint16_t * matrix_c,
                                                 uint16_t * processed_blocks_storage)
{   
    uint32_t line;
    for (line = slave_id; line < BLOCK_DIM; line += slaves_total)
    {
        slave_sum_block_line(
            processed_blocks_storage, b_col, a_line,
            matrix_c, line
        );
    }

    return 0;
}

int slave_process_image(uint32_t slave_id, uint32_t slaves_total,
                        uint16_t * matrix_a, uint16_t * matrix_b, uint16_t * matrix_c,
                        uint16_t * processed_blocks_storage)
{
    const uint32_t num_blocks_x = MATRIX_DIM / BLOCK_DIM;
    const uint32_t num_blocks_y = MATRIX_DIM / BLOCK_DIM;
    uint32_t sync_num = 1;

    for (uint32_t a_line = 0; a_line < num_blocks_y; a_line++)
    {
        for (uint32_t b_col = 0; b_col < num_blocks_x; b_col++)
        {
            slave_multiply_blocks_in_line(slave_id, slaves_total, a_line, b_col,
                                          matrix_a, matrix_b, matrix_c,
                                          processed_blocks_storage);
            slaves_barrier_sync(global_sync_arr, slave_id, slaves_total, sync_num);
            sync_num++;

            slave_sum_block_lines_in_block(slave_id, slaves_total, a_line, b_col,
                                           matrix_c, processed_blocks_storage);
            slaves_barrier_sync(global_sync_arr, slave_id, slaves_total, sync_num);
            sync_num++;
        }
    }

    return 0;
}

////////  Master functions  ///////////

int proces_image_on_slaves(int num_slaves, result_t *results_row, 
                           uint16_t *matrix_a, uint16_t *matrix_b, uint16_t *matrix_c, uint16_t *processed_blocks_storage)
{
    lwlog_info("Process image %s on %d slaves...", DATASET_NAME, num_slaves);

    uint64_t t_start_us, t_stop_us;
    uint64_t imem_start_cnt, dmem_start_cnt, total_start_cnt;
    uint64_t imem_stop_cnt, dmem_stop_cnt, total_stop_cnt;

    memset(global_sync_arr, 0, 128 * sizeof(uint32_t));

    malt_read_pcnt(&imem_start_cnt, &dmem_start_cnt, &total_start_cnt);
    t_start_us = malt_get_time_us();
    malt_start_pcnt();

    // Launch block multiplication on slaves
    for (int i = 0; i < num_slaves; i++)
    {
        int slave_id = malt_start_thr(slave_process_image, i, num_slaves,
                                      matrix_a, matrix_b, matrix_c,
                                      processed_blocks_storage);
        if (slave_id == -1)
        {
            lwlog_err("Faild to launch task %d", i);
            exit(-1);
        }
    }
    malt_sm_wait_all_lines_free();
    
    // Processing finished
    malt_stop_pcnt();
    t_stop_us = malt_get_time_us();
    lwlog_info("Processing done in %d us!", (uint32_t)(t_stop_us - t_start_us));

    malt_read_pcnt(&imem_stop_cnt, &dmem_stop_cnt, &total_stop_cnt); 
    results_row[num_slaves - 1].imem_cnt = (imem_stop_cnt - imem_start_cnt);
    results_row[num_slaves - 1].dmem_cnt = (dmem_stop_cnt - dmem_start_cnt);
    results_row[num_slaves - 1].total_cnt = (total_stop_cnt - total_start_cnt);
    results_row[num_slaves - 1].time_us = (uint32_t)(t_stop_us - t_start_us);
    
    #ifdef PRINT_RESULT_MATRIX
    print_matrix(matrix_a, MATRIX_DIM, MATRIX_DIM);
    printf("-----------\n");
    print_matrix(matrix_b, MATRIX_DIM, MATRIX_DIM);
    printf("-----------\n");
    print_matrix(matrix_c, MATRIX_DIM, MATRIX_DIM);
    printf("-----------\n");
    print_matrix(processed_blocks_storage, BLOCK_DIM * (MATRIX_DIM / BLOCK_DIM), BLOCK_DIM);
    #endif  // PRINT_RESULT_MATRIX

    
    // Проверка корректности результата
    uint32_t crc = 0;
    uint32_t crc_check_ok = 0;
#ifdef CHECK_CRC
    lwlog_info("Check CRC...");
    // MALT has an other byte order
    for (int i = 0; i < MATRIX_DIM * MATRIX_DIM; i++)
    {
        matrix_c[i] = ((matrix_c[i] & 0xff) << 8) | ((matrix_c[i] & 0xff00) >> 8);
    }
    
    crc = crc32(0, (uint8_t *)matrix_c, MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    crc_check_ok = (crc == RESULT_CRC32);

    if (crc_check_ok)
    {
        lwlog_info("CRC is correct. %x", crc);
    }
    else
    {
        lwlog_err("Wrong CRC!!!. Got %x. Expected %x\n", crc, RESULT_CRC32);
    }
    results_row[num_slaves - 1].crc = crc_check_ok;
#endif // CHECK_CRC

    // Вывод результатов
    char results[256];
    results[0] = '\0';
    sprintf(&results[strlen(results)], "### Sobel results ###\n");
    sprintf(&results[strlen(results)], "Image: %s\n", DATASET_NAME);
    sprintf(&results[strlen(results)], "Time_ms: %.2f\n", ((float)results_row[num_slaves - 1].time_us) / (1000));
    sprintf(&results[strlen(results)], "Iterations_done: 1\n");
    sprintf(&results[strlen(results)], "CRC: 0x%x\n", crc);
    sprintf(&results[strlen(results)], "CRC_valid: %s\n", crc_check_ok ? "True" : "False");
    sprintf(&results[strlen(results)], "Comment:");
    #ifdef READ_GLOBAL_MEM_ENABLE
    sprintf(&results[strlen(results)], " READ_GLOBAL_MEM_ENABLE");
    #endif
    #ifdef PROCESS_DATA_ENABLE
    sprintf(&results[strlen(results)], " PROCESS_DATA_ENABLE");
    #endif 
    #ifdef WRITE_GLOBAL_MEM_ENABLE
    sprintf(&results[strlen(results)], " WRITE_GLOBAL_MEM_ENABLE");
    #endif
    sprintf(&results[strlen(results)], "\n");
    sprintf(&results[strlen(results)], "Imem_cnt_e3: %d\n", (uint32_t)((imem_stop_cnt - imem_start_cnt) / 1000));
    sprintf(&results[strlen(results)], "Dmem_cnt_e3: %d\n", (uint32_t)((dmem_stop_cnt - dmem_start_cnt) / 1000));
    sprintf(&results[strlen(results)], "Total_cnt_e3: %d\n", (uint32_t)((total_stop_cnt - total_start_cnt) / 1000));
    sprintf(&results[strlen(results)], "\n");
    //rintf(results);
    
    printf("\n");

    return 0;
}



int main(void) 
{
    uint16_t *matrix_a, *matrix_b, *matrix_c, *processed_blocks_storage;

#ifdef MOVE_GLOBAL_BUFFERS_TO_HEAP
    matrix_a = (uint16_t *) memalign(MEM_ALIGNMENT, MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    matrix_b = (uint16_t *) memalign(MEM_ALIGNMENT, MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    matrix_c = (uint16_t *) memalign(MEM_ALIGNMENT, MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    processed_blocks_storage = (uint16_t *) memalign(MEM_ALIGNMENT, BLOCK_DIM * BLOCK_DIM * (MATRIX_DIM / BLOCK_DIM) * sizeof(uint16_t));

    memcpy(matrix_a, input_matrix_a, MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    memcpy(matrix_b, input_matrix_b, MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
#else  // MOVE_GLOBAL_BUFFERS_TO_HEAP
    matrix_a = input_matrix_a;
    matrix_b = input_matrix_b;
    matrix_c = static_matrix_c;
    processed_blocks_storage = static_processed_blocks_storage;
#endif  // MOVE_GLOBAL_BUFFERS_TO_HEAP
    malt_print_system_config();

    lwlog_info("Buffers allocated: %x %x %x %x", (uint32_t)matrix_a, (uint32_t)matrix_b, (uint32_t)matrix_c, (uint32_t)processed_blocks_storage);

    int num_slaves = malt_get_total_slaves();
    int super_master_id = malt_cur_core_num();
    lwlog_info("%d slaves found. Super master id: %x", num_slaves, super_master_id);

    result_t *results;
    results = malloc(num_slaves * sizeof(result_t));
    memset(results, 0, num_slaves * sizeof(result_t));
    
    for (int i = 0; i < num_slaves; i++)
    {
        proces_image_on_slaves(i + 1, results, matrix_a, matrix_b, matrix_c, processed_blocks_storage);
    }
    
    //num_slaves = 1;
    //proces_image_on_slaves(num_slaves, results, matrix_a, matrix_b, matrix_c, processed_blocks_storage);
    
    lwlog_info("Done!");
    
    printf("N\tdmem_cnt\ttotal_cnt\ttime_us\tcrc\n");
    for (int i = 0; i < num_slaves; i++)
    {
        printf("%d\t%d\t%d\t%d\t%x\n", 
               i + 1,
               results[i].dmem_cnt, 
               results[i].total_cnt, 
               results[i].time_us, 
               results[i].crc);
    }
    
    free(results);
    
#ifdef MOVE_GLOBAL_BUFFERS_TO_HEAP
    //free(matrix_a);
    //free(matrix_b);
    //free(matrix_c);
    //free(processed_blocks_storage);
#endif  // MOVE_GLOBAL_BUFFERS_TO_HEAP
    return 0;
}
