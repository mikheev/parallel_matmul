#ifndef __MALT_EXT_HEADER
#define __MALT_EXT_HEADER

#include "stdint.h"

// Struct for malt calculation statistics
typedef struct{
    int imem_cnt;
    int dmem_cnt;
    int total_cnt;
    int time_us;
    int crc;
} result_t;

/*
 * Barrier synchronization function for first num_slaves.
 * In case of multiple calls sync_num should be incremented for each call.
 * Otherwise synchronization will not happen.
 * Before the first call global_sync_arr should contain zeros 
 * and sync_num should be 1.
 */
static inline void slaves_barrier_sync(uint32_t * global_sync_arr, uint32_t slave_id, uint32_t num_slaves, uint32_t sync_num)
{
    uint32_t sum = 0;
    global_sync_arr[slave_id] = sync_num;
    while (sum < num_slaves * sync_num)
    {
        sum = 0;
        for (uint32_t i = 0; i < num_slaves; i++)
        {
            sum += global_sync_arr[i];
        }
    }
}

#endif // __MALT_EXT_HEADER