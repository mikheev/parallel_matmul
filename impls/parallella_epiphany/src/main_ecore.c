#include <e_bsp.h>
#include "stdint.h"

#include "../../../common/parallel_matmul_core.h"
#include "common.h"
#include "string.h"


static inline void ecore_multiply_blocks(volatile uint16_t * matrix_a, volatile uint16_t * matrix_b,
                                         uint32_t block_a_idx_x, uint32_t block_a_idx_y,
                                         uint32_t block_b_idx_x, uint32_t block_b_idx_y,
                                         volatile uint16_t * processed_blocks_storage)
{
    volatile uint16_t block_a[BLOCK_DIM * BLOCK_DIM];
    volatile uint16_t block_b[BLOCK_DIM * BLOCK_DIM];
    volatile uint16_t block_c[BLOCK_DIM * BLOCK_DIM];

    #ifdef ECORE_READ_SHARED_MEM_ENABLE
    for (uint32_t i = 0; i < BLOCK_DIM; i++)
    {
        const uint32_t src_a_offset = block_a_idx_y * BLOCK_DIM * MATRIX_DIM + block_a_idx_x * BLOCK_DIM;
        const uint32_t src_b_offset = block_b_idx_y * BLOCK_DIM * MATRIX_DIM + block_b_idx_x * BLOCK_DIM;
        ebsp_memcpy((void *)&block_a[i * BLOCK_DIM], 
                    (void *)&matrix_a[src_a_offset + i * MATRIX_DIM],
                    BLOCK_DIM * sizeof(uint16_t));
        ebsp_memcpy((void *)&block_b[i * BLOCK_DIM],
                    (void *)&matrix_b[src_b_offset + i * MATRIX_DIM],
                    BLOCK_DIM * sizeof(uint16_t));
    }
    #endif  // ECORE_READ_SHARED_MEM_ENABLE

    #ifdef PROCESS_DATA_ENABLE
    multiply_matrix(block_a, block_b, block_c, BLOCK_DIM, BLOCK_DIM, BLOCK_DIM);
    #endif  // PROCESS_DATA_ENABLE

    #ifdef ECORE_WRITE_SHARED_MEM_ENABLE
    for (uint32_t i = 0; i < BLOCK_DIM; i++)
    {
        const uint32_t dst_offset = block_a_idx_x * BLOCK_DIM;
        ebsp_memcpy((void *)&processed_blocks_storage[dst_offset + i * MATRIX_DIM],
                    (void *)&block_c[i * BLOCK_DIM],
                    BLOCK_DIM * sizeof(uint16_t));
    }
    #endif  // ECORE_WRITE_SHARED_MEM_ENABLE
}

static inline int ecore_multiply_blocks_in_line(int core_id, int cores_total, int a_line, int b_col,
                                                uint16_t * matrix_a, uint16_t * matrix_b, uint16_t * matrix_c,
                                                uint16_t * processed_blocks_storage)
{
    const uint32_t num_blocks_y = MATRIX_DIM / BLOCK_DIM;
    
    uint32_t block_n;
    for (block_n = core_id; block_n < num_blocks_y; block_n += cores_total)
    {
        ecore_multiply_blocks(
            matrix_a, matrix_b,
            block_n, a_line,
            b_col, block_n,
            processed_blocks_storage
        );
    }

    return 0;
}


static inline void ecore_sum_block_line(volatile uint16_t * processed_blocks_storage,
                                        uint32_t block_c_idx_x, uint32_t block_c_idx_y,
                                        volatile uint16_t * matrix_c,
                                        uint32_t block_line_idx_y)
{
    volatile uint16_t line[BLOCK_DIM];
    memset((void *) line, 0, BLOCK_DIM * sizeof(uint16_t));
    volatile uint16_t line_temp[BLOCK_DIM];

    for (uint32_t i = 0; i < MATRIX_DIM / BLOCK_DIM; i++)
    {
        #ifdef ECORE_READ_SHARED_MEM_ENABLE
        ebsp_memcpy((void *)line_temp,
                    (void *)&processed_blocks_storage[block_line_idx_y * MATRIX_DIM + i * BLOCK_DIM],
                    BLOCK_DIM * sizeof(uint16_t));
        #endif  // ECORE_READ_SHARED_MEM_ENABLE
        
        #ifdef PROCESS_DATA_ENABLE
        for (uint32_t j = 0; j < BLOCK_DIM; j++)
        {
            line[j] += line_temp[j];
        }
        #endif  // PROCESS_DATA_ENABLE
    }

    #ifdef ECORE_WRITE_SHARED_MEM_ENABLE
    ebsp_memcpy((void *)&matrix_c[(block_c_idx_y * BLOCK_DIM + block_line_idx_y) * MATRIX_DIM + block_c_idx_x * BLOCK_DIM],
                (void *)line,
                BLOCK_DIM * sizeof(uint16_t));
    #endif  // ECORE_WRITE_SHARED_MEM_ENABLE
}

static inline int ecore_sum_block_lines_in_block(int core_id, int cores_total, int a_line, int b_col,
                                                 uint16_t * matrix_c,
                                                 uint16_t * processed_blocks_storage)
{   
    uint32_t line;
    for (line = core_id; line < BLOCK_DIM; line += cores_total)
    {
        ecore_sum_block_line(
            processed_blocks_storage, b_col, a_line,
            matrix_c, line
        );
    }

    return 0;
}


int ecore_process_matrix(uint32_t core_id, uint32_t cores_total,
                         uint16_t * matrix_a, uint16_t * matrix_b, uint16_t * matrix_c,
                         uint16_t * processed_blocks_storage)
{
    const uint32_t num_blocks_x = MATRIX_DIM / BLOCK_DIM;
    const uint32_t num_blocks_y = MATRIX_DIM / BLOCK_DIM;

    for (uint32_t a_line = 0; a_line < num_blocks_y; a_line++)
    {
        for (uint32_t b_col = 0; b_col < num_blocks_x; b_col++)
        {
            ecore_multiply_blocks_in_line(core_id, cores_total, a_line, b_col,
                                          matrix_a, matrix_b, matrix_c,
                                          processed_blocks_storage);
            ebsp_barrier();

            ecore_sum_block_lines_in_block(core_id, cores_total, a_line, b_col,
                                           matrix_c, processed_blocks_storage);
            ebsp_barrier();
        }
    }

    return 0;
}


int main() {
    uint16_t *matrix_a = (uint16_t *)(EXT_MEM_BASE + EXT_MEM_MATRIX_A_OFFSET);
    uint16_t *matrix_b = (uint16_t *)(EXT_MEM_BASE + EXT_MEM_MATRIX_B_OFFSET);
    uint16_t *matrix_c = (uint16_t *)(EXT_MEM_BASE + EXT_MEM_MATRIX_C_OFFSET);
    uint16_t *processed_blocks_storage = (uint16_t *)(EXT_MEM_BASE + EXT_MEM_PROCESSED_BLOCKS_STORAGE_OFFSET);
    
    bsp_begin();

    ecore_process_matrix(bsp_pid(), bsp_nprocs(),
                         matrix_a, matrix_b, matrix_c, processed_blocks_storage);

    bsp_end();

    return 0;
}