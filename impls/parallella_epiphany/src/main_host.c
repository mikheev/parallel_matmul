#include <host_bsp.h>
#include <stdio.h>
#include "common.h"
#include "stdlib.h"
#include "e-hal.h"
#include "time.h"

#include "../../../common/parallel_matmul_core.h"
#include "../../../common/crc_check.h"
#include "../../../common/lwlog.h"


#define MEM_ALIGNMENT 4

uint16_t input_matrix_a[] __attribute__ ((aligned (MEM_ALIGNMENT))) = MATRIX_A;
uint16_t input_matrix_b[] __attribute__ ((aligned (MEM_ALIGNMENT))) = MATRIX_B;
uint16_t static_matrix_c[MATRIX_DIM * MATRIX_DIM] __attribute__ ((aligned (MEM_ALIGNMENT)));
uint16_t static_processed_blocks_storage[BLOCK_DIM * BLOCK_DIM * (MATRIX_DIM / BLOCK_DIM)] __attribute__ ((aligned (MEM_ALIGNMENT)));
float times_ms[32];


int proces_image_on_cores(int nprocs)
{
    lwlog_info("Process image on %d cores", nprocs);

    clock_t t_start_clk, t_end_clk;
    float time_ms;
    e_mem_t ext_mem_matrix_a, ext_mem_matrix_b, ext_mem_matrix_c, ext_mem_processed_blocks_storage;

    bsp_init("main_ecore.elf", 0, NULL);
    bsp_begin(nprocs);

    e_alloc(&ext_mem_matrix_a, EXT_MEM_MATRIX_A_OFFSET, MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    e_alloc(&ext_mem_matrix_b, EXT_MEM_MATRIX_B_OFFSET, MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    e_alloc(&ext_mem_matrix_c, EXT_MEM_MATRIX_C_OFFSET, MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    e_alloc(&ext_mem_processed_blocks_storage, EXT_MEM_PROCESSED_BLOCKS_STORAGE_OFFSET, BLOCK_DIM * BLOCK_DIM * (MATRIX_DIM / BLOCK_DIM) * sizeof(uint16_t));


    lwlog_info("Launch calculation");
    t_start_clk = clock();

    #ifdef HOST_WRITE_SHARED_MEM_ENABLE
    e_write(&ext_mem_matrix_a, 0, 0, 0, (void *)input_matrix_a, MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    e_write(&ext_mem_matrix_b, 0, 0, 0, (void *)input_matrix_b, MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    #endif  // HOST_WRITE_SHARED_MEM_ENABLE

    ebsp_spmd();

    #ifdef HOST_READ_SHARED_MEM_ENABLE
    e_read(&ext_mem_matrix_c, 0, 0, 0, (void *)static_matrix_c, MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    #endif  // HOST_READ_SHARED_MEM_ENABLE

    t_end_clk = clock();
    time_ms = ((float) (t_end_clk - t_start_clk) * 1000) / (CLOCKS_PER_SEC);
    lwlog_info("Processing done! Time spent: %.2f ms", time_ms);
    times_ms[nprocs] = time_ms;

    bsp_end(nprocs);


    #ifdef CHECK_CRC
    lwlog_info("Check CRC...");
    uint32_t crc = crc32(0, (uint8_t *)static_matrix_c, MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    int crc_check_ok = (crc == RESULT_CRC32);
    if (crc_check_ok)
    {
        lwlog_info("CRC is correct. %x", RESULT_CRC32);
    }
    else
    {
        lwlog_err("Wrong CRC!!!. Got %x. Expected %x\n", crc, RESULT_CRC32);
    }
    #endif  // CHECK_CRC

    e_free(&ext_mem_matrix_a);
    e_free(&ext_mem_matrix_b);
    e_free(&ext_mem_matrix_c);
    e_free(&ext_mem_processed_blocks_storage);

    return 0;
}

int main(int argc, char** argv)
{

    int num_slaves = 16;
    lwlog_info("%d cores available", num_slaves);
    //proces_image_on_coress(num_slaves);

    for (int i = 1; i <= num_slaves; i++)
    {
        proces_image_on_cores(i);
    }

    printf("N\tdmem_cnt\ttotal_cnt\ttime_us\tcrc\n");
    for (int i = 1; i <= num_slaves; i++)
    {
        printf("%d\t0\t0\t%d\t0\n", i, (int)(times_ms[i] * 1000));
    }

    return 0;
}