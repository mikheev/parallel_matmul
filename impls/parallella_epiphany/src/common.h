#ifndef COMMON_H_
#define COMMON_H_

#define EXT_MEM_BASE                             (0x8e000000)
#define EXT_MEM_MATRIX_A_OFFSET                  (0x00100000)
#define EXT_MEM_MATRIX_B_OFFSET                  (0x00700000)
#define EXT_MEM_MATRIX_C_OFFSET                  (0x01300000)
#define EXT_MEM_PROCESSED_BLOCKS_STORAGE_OFFSET  (0x01900000)

#define BLOCK_DIM          16

#define CHUNK_SIZE      ((PROC_REGION_SIZE_X + 4) * (PROC_REGION_SIZE_Y + 4))   // Words per processing element


// Calculation settings
#define HOST_WRITE_SHARED_MEM_ENABLE
#define ECORE_READ_SHARED_MEM_ENABLE
#define PROCESS_DATA_ENABLE
#define ECORE_WRITE_SHARED_MEM_ENABLE
#define HOST_READ_SHARED_MEM_ENABLE
#define CHECK_CRC


//#include "../../../test_data/matrix_32_squares.h"
#include "../../../test_data/matrix_512_random.h"

#endif  // COMMON_H_
