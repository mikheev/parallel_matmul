#include "stdio.h"
#include "stdbool.h"
#include "string.h"
#include "time.h"

#include "../../common/parallel_matmul_core.h"
#include "../../common/crc_check.h"

#define LOG_COLOR (0)
#include "../../common/lwlog.h"

//#include "../../test_data/matrix_16_lines.h"
//#include "../../test_data/matrix_32_squares.h"
#include "../../test_data/matrix_512_random.h"


#define NUM_ITERATIONS 100

//#define PRINT_RESULT_MATRIX

int main(void) 
{
    uint16_t input_matrix_a[] = MATRIX_A;
    uint16_t input_matrix_b[] = MATRIX_B;
    uint16_t *matrix_a, *matrix_b, *matrix_c;

    clock_t t_start, t_end;
    uint32_t iteration;

    // Все вычисденя должны производиться над данными из кучи. 
    // Результаты должны быть записаны туда же.
    // Поэтому нужно выделить соответствующие буфера и скопировать туда матрицы.
    matrix_a = (uint16_t *) malloc(MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    matrix_b = (uint16_t *) malloc(MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    matrix_c = (uint16_t *) malloc(MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));

    memcpy(matrix_a, input_matrix_a, MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    memcpy(matrix_b, input_matrix_b, MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));

    //---------- Обработка изображения ------------
    lwlog_info("Process image %s...", DATASET_NAME);
    t_start = clock();
    for (iteration = 0; iteration < NUM_ITERATIONS; iteration++)
    {
        multiply_matrix(matrix_a, matrix_b, matrix_c, MATRIX_DIM, MATRIX_DIM, MATRIX_DIM);
    }
    t_end = clock();
    lwlog_info("Processing done!");
    //---------- Конец обработки изображения ------------

    // Проверка корректности результата
    lwlog_info("Check CRC...");
    uint32_t crc = crc32(0, (uint8_t *)matrix_c, MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    bool crc_check_ok = (crc == RESULT_CRC32);

    if (crc_check_ok)
    {
        lwlog_info("CRC is correct. %x", crc, RESULT_CRC32);
    }
    else
    {
        lwlog_err("Wrong CRC!!!. Got %x. Expected %x\n", crc, RESULT_CRC32);
    }

    #ifdef PRINT_RESULT_MATRIX
    print_matrix(matrix_a, MATRIX_DIM, MATRIX_DIM);
    printf("-----------\n");
    print_matrix(matrix_b, MATRIX_DIM, MATRIX_DIM);
    printf("-----------\n");
    print_matrix(matrix_c, MATRIX_DIM, MATRIX_DIM);
    #endif  // PRINT_RESULT_MATRIX

    // Вывод результатов
    char results[256];
    results[0] = '\0';
    sprintf(&results[strlen(results)], "### Matrix multiplication results ###\n");
    sprintf(&results[strlen(results)], "Dataset: %s\n", DATASET_NAME);
    sprintf(&results[strlen(results)], "Time_ms: %.2f\n", ((float)(t_end - t_start) * 1000) / (NUM_ITERATIONS * CLOCKS_PER_SEC));
    sprintf(&results[strlen(results)], "Iterations_done: %d\n", NUM_ITERATIONS);
    sprintf(&results[strlen(results)], "CRC: 0x%x\n", crc);
    sprintf(&results[strlen(results)], "CRC_valid: %s\n", crc_check_ok ? "True" : "False");
    sprintf(&results[strlen(results)], "Comment: \n");
    sprintf(&results[strlen(results)], "\n");
    printf(results);

    free(matrix_a);
    free(matrix_b);
    free(matrix_c);

    return 0;
}