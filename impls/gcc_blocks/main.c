#include "stdio.h"
#include "stdbool.h"
#include "string.h"
#include "time.h"

#include "../../common/parallel_matmul_core.h"
#include "../../common/crc_check.h"

#define LOG_COLOR (0)
#include "../../common/lwlog.h"

//#include "../../test_data/matrix_16_lines.h"
//#include "../../test_data/matrix_32_squares.h"
#include "../../test_data/matrix_512_random.h"


#define BLOCK_DIM          8

#define NUM_ITERATIONS 100

//#define PRINT_RESULT_MATRIX

void multiply_blocks(uint16_t * matrix_a, uint16_t * matrix_b,
                     uint32_t block_a_idx_x, uint32_t block_a_idx_y,
                     uint32_t block_b_idx_x, uint32_t block_b_idx_y,
                     uint16_t * processed_blocks_storage)
{
    uint16_t block_a[BLOCK_DIM * BLOCK_DIM];
    uint16_t block_b[BLOCK_DIM * BLOCK_DIM];
    uint16_t block_c[BLOCK_DIM * BLOCK_DIM];

    for (uint32_t i = 0; i < BLOCK_DIM; i++)
    {
        const uint32_t src_a_offset = block_a_idx_y * BLOCK_DIM * MATRIX_DIM + block_a_idx_x * BLOCK_DIM;
        const uint32_t src_b_offset = block_b_idx_y * BLOCK_DIM * MATRIX_DIM + block_b_idx_x * BLOCK_DIM;
        memcpy(&block_a[i * BLOCK_DIM], &matrix_a[src_a_offset + i * MATRIX_DIM], BLOCK_DIM * sizeof(uint16_t));
        memcpy(&block_b[i * BLOCK_DIM], &matrix_b[src_b_offset + i * MATRIX_DIM], BLOCK_DIM * sizeof(uint16_t));
    }

    multiply_matrix(block_a, block_b, block_c, BLOCK_DIM, BLOCK_DIM, BLOCK_DIM);

    for (uint32_t i = 0; i < BLOCK_DIM; i++)
    {
        const uint32_t dst_offset = block_a_idx_x * BLOCK_DIM;
        memcpy(&processed_blocks_storage[dst_offset + i * MATRIX_DIM], &block_c[i * BLOCK_DIM], BLOCK_DIM * sizeof(uint16_t));
    }
}

void sum_block_line(uint16_t * processed_blocks_storage,
                    uint32_t block_c_idx_x, uint32_t block_c_idx_y,
                    uint16_t * matrix_c,
                    uint32_t block_line_idx_y)
{
    uint16_t line[BLOCK_DIM];
    memset((void *) line, 0, BLOCK_DIM * sizeof(uint16_t));
    uint16_t line_temp[BLOCK_DIM];

    for (uint32_t i = 0; i < MATRIX_DIM / BLOCK_DIM; i++)
    {
        memcpy(line_temp,
               &processed_blocks_storage[block_line_idx_y * MATRIX_DIM + i * BLOCK_DIM],
               BLOCK_DIM * sizeof(uint16_t));
        
        for (uint32_t j = 0; j < BLOCK_DIM; j++)
        {
            line[j] += line_temp[j];
        }
    }

    memcpy(&matrix_c[(block_c_idx_y * BLOCK_DIM + block_line_idx_y) * MATRIX_DIM + block_c_idx_x * BLOCK_DIM],
           line,
           BLOCK_DIM * sizeof(uint16_t));
}

int main(void) 
{
    uint16_t input_matrix_a[] = MATRIX_A;
    uint16_t input_matrix_b[] = MATRIX_B;
    uint16_t *matrix_a, *matrix_b, *matrix_c;
    uint16_t *processed_blocks_storage;

    clock_t t_start, t_end;
    uint32_t iteration;

    // Все вычисденя должны производиться над данными из кучи. 
    // Результаты должны быть записаны туда же.
    // Поэтому нужно выделить соответствующие буфера и скопировать туда матрицы.
    matrix_a = (uint16_t *) malloc(MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    matrix_b = (uint16_t *) malloc(MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    matrix_c = (uint16_t *) malloc(MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    processed_blocks_storage = (uint16_t *) malloc(BLOCK_DIM * BLOCK_DIM * (MATRIX_DIM / BLOCK_DIM) * sizeof(uint16_t));

    memcpy(matrix_a, input_matrix_a, MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    memcpy(matrix_b, input_matrix_b, MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));

    //---------- Обработка изображения ------------
    lwlog_info("Process image %s...", DATASET_NAME);
    t_start = clock();
    for (iteration = 0; iteration < NUM_ITERATIONS; iteration++)
    {
        const uint32_t num_blocks_x = MATRIX_DIM / BLOCK_DIM;
        const uint32_t num_blocks_y = MATRIX_DIM / BLOCK_DIM;

        for (uint32_t a_line = 0; a_line < num_blocks_y; a_line++)
        {
            for (uint32_t b_col = 0; b_col < num_blocks_x; b_col++)
            {
                for (uint32_t block_n = 0; block_n < num_blocks_y; block_n++)
                {
                    multiply_blocks(
                        matrix_a, matrix_b,
                        block_n, a_line,
                        b_col, block_n,
                        processed_blocks_storage
                    );
                }

                for (uint32_t i = 0; i < BLOCK_DIM; i++)
                {
                    sum_block_line(
                        processed_blocks_storage, b_col, a_line,
                        matrix_c, i
                    );
                }
            }
        }
    }
    t_end = clock();
    lwlog_info("Processing done!");
    //---------- Конец обработки изображения ------------

    // Проверка корректности результата
    lwlog_info("Check CRC...");
    uint32_t crc = crc32(0, (uint8_t *)matrix_c, MATRIX_DIM * MATRIX_DIM * sizeof(uint16_t));
    bool crc_check_ok = (crc == RESULT_CRC32);

    if (crc_check_ok)
    {
        lwlog_info("CRC is correct. %x", crc, RESULT_CRC32);
    }
    else
    {
        lwlog_err("Wrong CRC!!!. Got %x. Expected %x\n", crc, RESULT_CRC32);
    }

    #ifdef PRINT_RESULT_MATRIX
    print_matrix(matrix_a, MATRIX_DIM, MATRIX_DIM);
    printf("-----------\n");
    print_matrix(matrix_b, MATRIX_DIM, MATRIX_DIM);
    printf("-----------\n");
    print_matrix(matrix_c, MATRIX_DIM, MATRIX_DIM);
    printf("-----------\n");
    print_matrix(processed_blocks_storage, BLOCK_DIM * (MATRIX_DIM / BLOCK_DIM), BLOCK_DIM);
    #endif  // PRINT_RESULT_MATRIX

    // Вывод результатов
    char results[256];
    results[0] = '\0';
    sprintf(&results[strlen(results)], "### Matrix multiplication results ###\n");
    sprintf(&results[strlen(results)], "Dataset: %s\n", DATASET_NAME);
    sprintf(&results[strlen(results)], "Time_ms: %.2f\n", ((float)(t_end - t_start) * 1000) / (NUM_ITERATIONS * CLOCKS_PER_SEC));
    sprintf(&results[strlen(results)], "Iterations_done: %d\n", NUM_ITERATIONS);
    sprintf(&results[strlen(results)], "CRC: 0x%x\n", crc);
    sprintf(&results[strlen(results)], "CRC_valid: %s\n", crc_check_ok ? "True" : "False");
    sprintf(&results[strlen(results)], "Comment: \n");
    sprintf(&results[strlen(results)], "\n");
    printf(results);

    free(matrix_a);
    free(matrix_b);
    free(matrix_c);
    free(processed_blocks_storage);

    return 0;
}